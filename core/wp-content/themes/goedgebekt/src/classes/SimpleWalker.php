<?php 

namespace Goedgebekt;


use Roots\Soil\Utils;


class SimpleWalker extends \Roots\Soil\Nav\NavWalker
{
  function start_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul role=\"menu\" class=\" dropdown-menu\">\n";
  }
}