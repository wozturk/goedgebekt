<?php

/*
* @author: wozturk
* @package: goedgebekt
 */

namespace Goedgebekt;


  function register_documents_cpt()
  {
      $labels = array(
          'name'               => _x( 'Documents', 'post type general name', 'sage' ),
          'singular_name'      => _x( 'Document', 'post type singular name', 'sage' ),
          'menu_name'          => _x( 'Documenten', 'admin menu', 'sage' ),
          'name_admin_bar'     => _x( 'Document', 'add new on admin bar', 'sage' ),
          'add_new'            => _x( 'Nieuwe Document', 'Document', 'sage' ),
          'add_new_item'       => __( 'Nieuwe Document', 'sage' ),
          'new_item'           => __( 'Nieuwe Document', 'sage' ),
          'edit_item'          => __( 'Wijzig Document', 'sage' ),
          'view_item'          => __( 'Bekijk Document', 'sage' ),
          'all_items'          => __( 'Alle Documents', 'sage' ),
          'search_items'       => __( 'Zoeken Documenten', 'sage' ),
          'parent_item_colon'  => __( 'Parent Documenten:', 'sage' ),
          'not_found'          => __( 'Geen Documenten gevonden.', 'sage' ),
          'not_found_in_trash' => __( 'Geen Documenten gevonden in de prullenbak.', 'sage' )
      );

      $args = array(
          'labels'             => $labels,
          'description'        => __( 'Omschrijving.', 'sage' ),
          'public'             => true,
          'publicly_queryable' => true,
          'show_ui'            => true,
          'show_in_menu'       => true,
          'query_var'          => true,
          'rewrite'            => array( 'slug' => 'documenten' ),
          'capability_type'    => 'post',
          'has_archive'        => false,
          'hierarchical'       => false,
          'menu_position'      => null,
          'menu_icon'           => 'dashicons-groups',
          'supports'           => array( 'title', 'editor', 'custom-fields' )
      );

      register_post_type( 'documents', $args );
  }
