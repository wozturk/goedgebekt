<?php

/*
* @author: wozturk
* @package: goedgebekt
 */

namespace Goedgebekt;


  function register_arrangements_cpt()
  {
      $labels = array(
          'name'               => _x( 'Arrangements', 'post type general name', 'sage' ),
          'singular_name'      => _x( 'Arrangement', 'post type singular name', 'sage' ),
          'menu_name'          => _x( 'Arrangementen', 'admin menu', 'sage' ),
          'name_admin_bar'     => _x( 'Arrangement', 'add new on admin bar', 'sage' ),
          'add_new'            => _x( 'Nieuwe Arrangement', 'Arrangement', 'sage' ),
          'add_new_item'       => __( 'Nieuwe Arrangement', 'sage' ),
          'new_item'           => __( 'Nieuwe Arrangement', 'sage' ),
          'edit_item'          => __( 'Wijzig Arrangement', 'sage' ),
          'view_item'          => __( 'Bekijk Arrangement', 'sage' ),
          'all_items'          => __( 'Alle Arrangementen', 'sage' ),
          'search_items'       => __( 'Zoeken Arrangementen', 'sage' ),
          'parent_item_colon'  => __( 'Parent Arrangements:', 'sage' ),
          'not_found'          => __( 'Geen Arrangementen gevonden.', 'sage' ),
          'not_found_in_trash' => __( 'Geen Arrangementen gevonden in de prullenbak.', 'sage' )
      );

      $args = array(
          'labels'             => $labels,
          'description'        => __( 'Omschrijving.', 'sage' ),
          'public'             => true,
          'publicly_queryable' => true,
          'show_ui'            => true,
          'show_in_menu'       => true,
          'query_var'          => true,
          'rewrite'            => array( 'slug' => 'arrangementen' ),
          'capability_type'    => 'post',
          'has_archive'        => false,
          'hierarchical'       => false,
          'menu_position'      => null,
          'menu_icon'          => 'dashicons-playlist-audio',
          'taxonomies'         => array('category', 'post_tag'),
          'supports'           => array( 'title', 'editor', 'thumbnail', 'custom-fields' )
      );

      register_post_type( 'arrangements', $args );
  }
