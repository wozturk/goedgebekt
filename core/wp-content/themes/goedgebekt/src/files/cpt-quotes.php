<?php

/*
* @author: wozturk
* @package: goedgebekt
 */

namespace Goedgebekt;


  function register_quotes_cpt()
  {
      $labels = array(
          'name'               => _x( 'Quotes', 'post type general name', 'sage' ),
          'singular_name'      => _x( 'Quote', 'post type singular name', 'sage' ),
          'menu_name'          => _x( 'Quotes', 'admin menu', 'sage' ),
          'name_admin_bar'     => _x( 'Quote', 'add new on admin bar', 'sage' ),
          'add_new'            => _x( 'Nieuwe quote', 'quote', 'sage' ),
          'add_new_item'       => __( 'Nieuwe quote', 'sage' ),
          'new_item'           => __( 'Nieuwe quote', 'sage' ),
          'edit_item'          => __( 'Wijzig quote', 'sage' ),
          'view_item'          => __( 'Bekijk quote', 'sage' ),
          'all_items'          => __( 'Alle quotes', 'sage' ),
          'search_items'       => __( 'Zoeken quotes', 'sage' ),
          'parent_item_colon'  => __( 'Parent quotes:', 'sage' ),
          'not_found'          => __( 'Geen quotes gevonden.', 'sage' ),
          'not_found_in_trash' => __( 'Geen quotes gevonden in de prullenbak.', 'sage' )
      );

      $args = array(
          'labels'             => $labels,
          'description'        => __( 'Omschrijving.', 'sage' ),
          'public'             => true,
          'publicly_queryable' => true,
          'show_ui'            => true,
          'show_in_menu'       => true,
          'query_var'          => true,
          'rewrite'            => array( 'slug' => 'quotes' ),
          'capability_type'    => 'post',
          'has_archive'        => false,
          'hierarchical'       => false,
          'menu_position'      => null,
          'menu_icon'          => 'dashicons-format-quote',
          'supports'           => array( 'title', 'editor' )
      );

      register_post_type( 'quotes', $args );
  }
