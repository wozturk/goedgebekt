<?php 


//register CPTs
add_action('init', '\\Goedgebekt\\register_arrangements_cpt');
add_action('init', '\\Goedgebekt\\register_commissions_cpt');
add_action('init', '\\Goedgebekt\\register_documents_cpt');
add_action('init', '\\Goedgebekt\\register_quotes_cpt');

add_action('init', '\\Goedgebekt\\user_roles');
add_action('user_register', '\\Goedgebekt\\initiate_user', 11);



function add_specific_menu_location_atts( $atts, $item, $args ) {

  if( $args->theme_location == 'left_navigation' || $args->theme_location == 'right_navigation' ) {
    $atts['data-target'] = 'toggle-' . $item->ID;
    $atts['data-scroll'] = true;
  }
  
  return $atts;
}

add_filter( 'nav_menu_link_attributes', 'add_specific_menu_location_atts', 10, 3 );


# Override ajax loader

add_filter('wpcf7_ajax_loader', function() {
    return  get_template_directory_uri() . '/dist/images/ajax-loader.gif';
});

# Send users to the dashboard page after using ajax login form
add_filter('harperjones/login/redirecturl', function($url) {
  return '/dashboard';
});




//Register Theme options
if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page([
    'page_title'  => 'Theme Setup',
    'menu_title'  => 'Goed Gebekt Gegevens',
    'menu_slug'   => 'theme-options',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ]);
}




/*
 * wp_generate_tag_cloud is called after wp_tagcloud has generated the whole output. It has 3 parameters:
 * string $output: the complete tagcloud as HTML
 * array  $tags:   an array containing all tags that make up the cloud (in their respective order)
 * array  $args:   a list of arguments passed to the wp_tagcloud function.
 *
 * We can use this filter to modify the outpit of the tagcloud. In this case, we want to add a data-group attribute
 * to indicate which group should be filtered on.
 *
 * We could also use the wp_tag_cloud filter, but we would be missing the tag information. Not crucial, but having
 * that, makes manipulating the output MUCH easier (see below).
 */
add_filter('wp_generate_tag_cloud', function($output,$tags,$args) {

  // We can do two things: build the whole HTML again by itself, or modify the code wordpress gives us.
  // Since building the tagcloud by ourself again, because we will need to calculate the font size again.
  // So what we do, is we search the link, and replace it with the link and our data-group attribute that
  // we want to add.
  foreach( $tags as $tag ) {
    $href   = 'href=\'' . $tag->link . '\'';
    $output = str_replace($href,$href . ' data-group="' . $tag->slug . '"',$output);
  }

  return $output;
},10,3);