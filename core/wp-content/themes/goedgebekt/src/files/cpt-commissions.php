<?php

/*
* @author: wozturk
* @package: goedgebekt
 */

namespace Goedgebekt;


  function register_commissions_cpt()
  {
      $labels = array(
          'name'               => _x( 'Commissions', 'post type general name', 'sage' ),
          'singular_name'      => _x( 'Commissie', 'post type singular name', 'sage' ),
          'menu_name'          => _x( 'Commissies', 'admin menu', 'sage' ),
          'name_admin_bar'     => _x( 'Commissie', 'add new on admin bar', 'sage' ),
          'add_new'            => _x( 'Nieuwe Commissie', 'Commissie', 'sage' ),
          'add_new_item'       => __( 'Nieuwe Commissie', 'sage' ),
          'new_item'           => __( 'Nieuwe Commissie', 'sage' ),
          'edit_item'          => __( 'Wijzig Commissie', 'sage' ),
          'view_item'          => __( 'Bekijk Commissie', 'sage' ),
          'all_items'          => __( 'Alle Commissies', 'sage' ),
          'search_items'       => __( 'Zoeken Commissies', 'sage' ),
          'parent_item_colon'  => __( 'Parent Commissies:', 'sage' ),
          'not_found'          => __( 'Geen Commissies gevonden.', 'sage' ),
          'not_found_in_trash' => __( 'Geen Commissies gevonden in de prullenbak.', 'sage' )
      );

      $args = array(
          'labels'             => $labels,
          'description'        => __( 'Omschrijving.', 'sage' ),
          'public'             => true,
          'publicly_queryable' => true,
          'show_ui'            => true,
          'show_in_menu'       => true,
          'query_var'          => true,
          'rewrite'            => array( 'slug' => 'commissies' ),
          'capability_type'    => 'post',
          'has_archive'        => false,
          'hierarchical'       => false,
          'menu_position'      => null,
          'menu_icon'           => 'dashicons-groups',
          'supports'           => array( 'title', 'editor', 'custom-fields' )
      );

      register_post_type( 'commissions', $args );
  }
