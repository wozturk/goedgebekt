<?php 
/**
 * Return user id
 *
 */


function get_user_id() 
{
  if (get_the_ID()) {
    $userId = 'user_' . get_the_ID();
    return $userId;
  }
}

/**
 * Return user image
 *
 */
function get_user_image($id) 
{
  var_dump(get_the_ID());
    $profileImg = get_field('portret_foto', 'user_' . $id);
    return $profileImg;
}
/**
 * Return commissions user is part of, can be multiple
 *
 */
function get_user_commissions()
{

}
/**
 * Return arrangements user has access to
 *
 */
function get_user_arrangements()
{

}
/**
 * Return user vocal range
 *
 */

function get_vocal_ranges()
{
  $fieldKey = 'field_57ab8321efa86';
  $field = get_field_object($fieldKey);

  // PE: De list heeft een key en een value.. de value is wat weergegeven wordt, de key is wat opgeslagen wordt in de database.
  // Soms zul je de ene nodig hebben, soms de andere. Dus ik geef ze liever allebei terug. (als je dat niet had gewild had je
  // in plaats van een loop ook array_values($field['choices']) kunnen doen.
  return $field['choices'];
}

function get_user_vocalrange($id)
{
  $vocalRange =  get_field('voice_type', 'user_' . $id);
  return $vocalRange;
}
/**
 * Return user bio description
 *
 */
function get_user_bio() 
{

}

/**
 * Return users by vocal range
 * @param vocalrange
 *
 */
function get_users_by_vocalrange($vocalrange)
{
    $args = array(
    'blog_id'      => $GLOBALS['blog_id'],
    'role'         => '',
    'role__in'     => array(),
    'role__not_in' => array('entry-level-user'),
    'meta_key'     => '',
    'meta_value'   => '',
    'meta_compare' => '',
    'meta_query'   => array(
      array(
          'key' => 'voice_type',
          'value' => $vocalrange
        )

      ),
    'date_query'   => array(),        
    'include'      => array(),
    'exclude'      => array(),
    'orderby'      => 'login',
    'order'        => 'ASC',
    'offset'       => '',
    'search'       => '',
    'number'       => '',
    'count_total'  => false,
    'fields'       => 'all',
    'who'          => '',
   ); 

  $users = get_users( $args );

  return $users;
}


/**
 * Obtain a list of users that belong to a specific vocal range
 * Please use the internal values only!
 *
 * @param  string $vocalranges
 * @return array
 */
function get_users_sorted_by_vocalrange($vocalranges) 
{
  $list = []; 
  foreach ($vocalranges as $vocalrange) {
    $list  = array_merge($list,get_users_by_vocalrange($vocalrange));
  }
  return $list;
}

function get_users_grouped_by_vocalrange($vocalranges)
{
  $list = []; 
  foreach ($vocalranges as $vocalrange) {
    $list[$vocalrange] = get_users_by_vocalrange($vocalrange);
  }
  return $list;
}

