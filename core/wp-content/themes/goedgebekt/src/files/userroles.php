<?php  

namespace Goedgebekt;

function user_roles()
{
  //still need to set these!
  add_role('entry-level-user', 'Nieuw lid', array(
          'read'         => true,  // true allows this capability
          'edit_posts'   => true,
          'delete_posts' => false, // Use false to explicitly deny
          ));

  add_role('trial-level-user', 'Lid in proefperiode', array(
          'read'         => true,  // true allows this capability
          'edit_posts'   => true,
          'delete_posts' => false, // Use false to explicitly deny
          ));

  add_role('senior-level-user', 'Lid', array(
          'read'         => true,  // true allows this capability
          'edit_posts'   => true,
          'delete_posts' => false, // Use false to explicitly deny
          ));

  add_role('power-level-user', 'Lid en Admin', array(
          'read'         => true,  // true allows this capability
          'edit_posts'   => true,
          'delete_posts' => false, // Use false to explicitly deny
          ));

}

// Hook into the registration process
// function initiate_user($user_id)
// {
//     $user = new WP_User($user_id);

//     // Remove all user roles after registration
//     foreach($user->roles as $role){
//         $user->remove_role($role);
//     }
// }