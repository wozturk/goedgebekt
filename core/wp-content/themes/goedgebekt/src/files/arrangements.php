<?php 

$_arrangement_cats = false;

/**
 * Return arrangement id
 *
 * 
 */

function get_arrangement_id()
{

}

/**
 * Return arrangement title
 *
 * @param int | $id
 * @return string
 */
function get_arrangement_title($id = false)
{

}

/**
 * Return arrangement artist
 *
 */
function get_arrangement_artist()
{

}

/**
 * Return arrangement album art
 *
 */
function get_arrangement_album()
{

}

/**
 * Return arrangement sheetmusic
 *
 */
function get_arrangement_pdf()
{

}
/**
 * Return arrangement cats
 *
 */
function get_arrangement_cats($post_id = false)
{

  return wp_get_post_terms( $post_id, 'category', array('fields' => 'names'));
  
}
/**
 * Return arrangement mp3 by vocalrange, default all
 *
 */
function get_arrangement_by_vocalrange($range)
{

}

/**
 * Return arrangement category terms
 * @return array
 */
function get_available_cats($taxonomy = 'category', $exclude = '1')
{
  global $_arrangement_cats;

  if (isset($_arrangement_cats)) {
   return $_arrangement_cats;
  }

  $terms = get_terms( array(
              'taxonomy' => $taxonomy,
              'exclude' => $exclude
            ));

  $final = [];

  foreach ($terms as $term) {
    $final[$term->slug] = $term->name;
  }

  $_arrangement_cats =  $final;

  return $_arrangement_cats;
}

function get_arrangements_with_cat()
{

  $terms = get_available_cats();

  $posts = get_posts( array(
            'post_type' => 'arrangements', 
            'posts_per_page' => -1,
            'category_name' => implode(',',array_keys($terms))
          ));
  shuffle($posts);
  return $posts;
}


/**
 * Return arrangements by userrole
 *
 */
function get_arrangements_by_userrole($userrole)
{

}

