<?php $args = array(
  'blog_id'      => $GLOBALS['blog_id'],
  'role'         => '',
  'role__in'     => array(),
  'role__not_in' => array(),
  'meta_key'     => '',
  'meta_value'   => '',
  'meta_compare' => '',
  'meta_query'   => array(),
  'date_query'   => array(),        
  'include'      => array(),
  'exclude'      => array(),
  'orderby'      => 'login',
  'order'        => 'ASC',
  'offset'       => '',
  'search'       => '',
  'number'       => '',
  'count_total'  => false,
  'fields'       => 'all',
  'who'          => ''
 ); 
$members = get_users( $args ); ?>

<?php
foreach ( $members as $member ) : 
  $email = esc_html( $member->user_email );
  $displayName = get_avatar_url($member->ID);
  $bio = get_the_author_meta('description', $member->ID);
?>
<?php endforeach; ?>
<div class="col-xs-12 col-sm-6 col-md-5" data-focus-parent='4squares' data-target="4squares">
  <div class="card-deck">
    <div class="row content">

     <div class="cards col-xs-6" data-focus='4squares' data-animate="slideInUp"> 
        <h2 class="cards__title">Sopranen</h2>
        <div class="card" data-focus-expand>
        <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/1" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Suzanne</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
              
          </div>
          </div>
        </div>


        <div class="card" data-focus-expand>
        <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/4" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Linda</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>
                <div class="card" data-focus-expand>
                <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/3" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Esther</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>
                <div class="card" data-focus-expand>
                <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/2" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Jackie</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>
      </div>

      
      <div class="cards col-xs-6" data-focus='4squares' data-animate="slideInUp"> 
        <h2 class="cards__title">Mezzo's</h2>
        <div class="card" data-focus-expand>
        <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/1" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Joyce</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>


        <div class="card" data-focus-expand>
        <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/4" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Angelique</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>
                <div class="card" data-focus-expand>
                <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/3" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Danielle</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>
                <div class="card" data-focus-expand>
                <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/2" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Helen</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>
      </div>

     <div class="cards col-xs-6" data-focus='4squares' data-animate="slideInUp"> 
        <h2 class="cards__title">Hoge Alten</h2>
        <div class="card" data-focus-expand>
        <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/1" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Cordina</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>


        <div class="card" data-focus-expand>
        <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/4" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Silvia</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>
                <div class="card" data-focus-expand>
                <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/3" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Lorance</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>
                <div class="card" data-focus-expand>
                <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/2" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Celeste</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>
      </div>


     <div class="cards col-xs-6" data-focus='4squares' data-animate="slideInUp"> 
        <h2 class="cards__title">Lage Alten</h2>
        <div class="card" data-focus-expand>
        <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/1" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Petra</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>


        <div class="card" data-focus-expand>
        <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/4" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Renata</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>
                <div class="card" data-focus-expand>
                <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/3" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Wieteke</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>
                <div class="card" data-focus-expand>
                <div class="content">
          <div class="card__header">
            <div class="card__avatar">
              <picture>
                <img class="card__img" data-src="http://lorempixel.com/400/400/cats/2" alt="display name">
              </picture>
            </div>
            <div class="card__meta">
              <h3>Karen</h3>
              <p class="">lid sinds <span>14 maart 1879</span></p>
            </div>
          </div>
          <div class="card__body">
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
<button class="btn btn--txt btn--dismiss" data-focus-reset>X</button>
</div>

<div class="wp-editor-content col-sm-6 pull-up">
<div class="row">
  <div class="col-sm-12" data-animate="fadeIn">
      <?php the_content(); ?>
  </div>  
</div>


<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
