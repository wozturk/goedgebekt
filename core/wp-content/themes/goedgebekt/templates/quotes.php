<?php 
$quotes = get_posts( array(
            'post_type' => 'quotes', 
            'posts_per_page' => -1,
            'orderby' > 'rand'
          ));
 ?>


<section class="" data-carousel>

<?php foreach ($quotes as $quote): ?>
  <div class="quote">
  <div class="quote__item">
    <q><?php echo $quote->post_content; ?></q>
    <div class="quote__origin"><?php echo $quote->post_title; ?></div>
  </div>

  </div>

<?php endforeach; ?>

</section>

  



