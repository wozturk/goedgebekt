<?php
  $rightColumn = get_field('content_column');
  $columnImageId = get_field('column_image');
  $columnImg = new HarperJones\Wordpress\Media\Attachment($columnImageId, 'full');
?>


<div class="col-sm-6">
  <?php the_content(); ?>
</div>

<?php
  if (!get_field('toon_image')) :?>
    <div class="col-sm-6">
      <?= $rightColumn; ?>  
    </div>
 <?php else: ?>

  <div class="img__frame col-sm-6 pull-up">
    <div class="img--framed">
      <div class="content">
        <?php echo $columnImg->displayImg('full', ['img--round'], true); ?>
      </div>
    </div>
    <svg  xmlns="http://www.w3.org/2000/svg">
      <circle cx="43%" cy="42%" r="36%" fill="#EB008B" data-animate="rotateIn"/>
      <circle cx="58%" cy="41%" r="33%" fill="#fe265e" data-animate="rotateIn"/>
      <circle cx="45%" cy="60%" r="34%" fill="#3beca8" data-animate="rotateIn"/>
      <circle cx="58%" cy="60%" r="32%" fill="#a9f946" data-animate="rotateIn"/>
    </svg>
  </div>
<?php endif; ?>


