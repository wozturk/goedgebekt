<?php 

// De get_vocal_ranges geeft (nu) een lijst van interne (key) en display (value) waardes. Omdat dingen opgeslagen
// worden met de interne waardes, gebruik ik alleen de keys via (array_keys()). Die heeft mij een lijst van alle
// keys in de array.
$users = get_users_sorted_by_vocalrange(array_keys(get_vocal_ranges()));
$usersGrouped = get_users_grouped_by_vocalrange(array_keys(get_vocal_ranges()));

// En dan loopen we er doorheen. Je had het ook met een loop kunnen doen.. dan zou het zoiets worden:
// loop($users)->each(function($user){
//    ....
// });


 //foreach( $usersGrouped as $currentGroup => $userGroup): ?>

<!-- <div class="users--<?php echo $currentGroup; ?>"> -->

  <?php foreach( $users as $user):

   $profileImg = get_field('portret_foto','user_' . $user->ID);
   ?>
   <figure class="member--<?php echo get_user_vocalrange($user->ID); ?>"><img src="<?php echo $profileImg; ?>"><figcaption><?php echo $user->display_name; ?></figcaption></figure>
   <?php endforeach; ?> 

<!-- </div> -->
<?php //endforeach; ?>



<div class="wp-editor-content col-sm-6 pull-up">
<div class="row">
  <div class="col-sm-12" data-animate="fadeInDown">
      <?php the_content(); ?>
  </div>  
</div>


<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
