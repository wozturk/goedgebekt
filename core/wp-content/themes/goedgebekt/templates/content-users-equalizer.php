<?php 

$featuredImg = new HarperJones\Wordpress\Media\FeaturedImage($post->ID);
$usersGrouped = get_users_grouped_by_vocalrange(array_keys(get_vocal_ranges()));
?>

<div class="wp-editor-content col-sm-6 pull-up">
  <div class="row">
    <div class="col-sm-12" data-animate="fadeInDown">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
    </div>  
  </div>
</div>

<div class="img__frame col-sm-6">
  <div class="img--framed">
    <div class="content">
      <?php $featuredImg->displayPicture(['img--round'],'Het Koor', 'full'); ?>
    </div>
  </div>
  <svg  xmlns="http://www.w3.org/2000/svg">
    <circle cx="43%" cy="42%" r="36%" fill="#EB008B"/>
    <circle cx="58%" cy="41%" r="33%" fill="#fe265e"/>
    <circle cx="45%" cy="60%" r="34%" fill="#3beca8"/>
    <circle cx="58%" cy="60%" r="32%" fill="#a9f946"/>
  </svg>
</div>
<!-- 
<?php loop($usersGrouped)->each(function($obj, $key) {

  echo "<div class='col-md-3 users--" . $key . "'><div class='row'>";

  loop($obj)->each(function($item) {
      $profileImg = get_field('portret_foto','user_' . $item->ID);

      echo "<figure class='col-md-3 member--" . get_user_vocalrange($item->ID) . "'><img src='" . $profileImg . "'><figcaption>" . $item->display_name . "</figcaption></figure>";
  });
   echo "</div></div>";

}); ?> -->

