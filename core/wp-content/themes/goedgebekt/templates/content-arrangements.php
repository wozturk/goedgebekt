<?php
  $arrangements = get_arrangements_with_cat();
  $terms = get_available_cats();
?>


<div class="list list--figure row" data-filter>
  <div class="tag-cloud">
    <?php wp_tag_cloud( $args ); ?>
  </div>

  <div class="list__items col-xs-12">
    <ul class="row" data-shuffle>
      <?php foreach( $arrangements as $arrangement):
        $album_art = new HarperJones\Wordpress\Media\FeaturedImage($arrangement->ID); ?>
          <?php
            $tags = [];
            $posttags = get_the_tags($arrangement->ID);
            if ($posttags) {
              foreach($posttags as $tag) {
                array_push($tags , $tag->slug); 
              }
            }
            
          ?>
      <li class="list__item col-xs-6 col-sm-6 col-md-4"  data-groups=<?php echo json_encode(array_values($tags)); ?>> 
        <div class="list__content row">
          <div class="list__figure col-xs-4 col-sm-3"><img class="content" src="<?php echo $album_art->getUrl() ?>" alt=""></div>

          <div class="list__details  col-xs-8 col-sm-9">
            <h3 class="list__title"><?php echo $arrangement->post_title; ?></h3>
            <p class="list__meta hidden-sm-down"><?php echo get_field('artist',$arrangement->ID ); ?></p>
          </div>
            <?php if (get_field('link_spotify' || get_field('link_sample'))): ?>
              
              <div class="list__cta">

                <div class="col-xs-2 col-sm-3"><span class="icon-play"></span></div>
                <div class="list__links col-xs-10 col-sm-9">
                  <?php if (get_field('link_sample')): ?>
                    <a class="list__link link--youtube" href="<?php get_field('link_sample'); ?>" data-icon><span>fragment</span></a>
                    
                  <?php endif ?>
                  <?php if (get_field('link_spotify')): ?>
                    
                  <a class="list__link link--spotify" href="<?php echo get_field('link_spotify'); ?>" data-icon><span>Spotify</span></a>
                  <?php endif ?>
              
                </div>  

              </div>
            <?php endif ?>
        </div>
      </li>
    <?php endforeach; ?>
          
    </ul>
  </div>
</div>
