<aside class="meta-info">
  <a href="<?php the_field('facebook', 'option') ?>" class="link--icon icon-facebook" target="_blank"><span class="sr-only">volg ons via facebook</span></a>
</aside>
<footer class="site-info"><?php the_field('site_info', 'option') ?></footer>