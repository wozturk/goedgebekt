<?php use Goedgebekt\SimpleWalker; ?>
<header class="header header--floating" data-anm-on>
  <div class="banner container">
    <nav class="nav--split nav--toggle" data-focus-parent="toggle-mob-nav">
      <button type="button" class="btn btn--square btn--pink btn--hamburger-cross" data-focus="toggle-mob-nav" data-target="toggle-menu-info">
        <span>toggle menu</span>
      </button>
      <button type="button" class="btn btn--square btn--pink btn--phone" data-focus="toggle-mob-nav" data-target="toggle-menu-contact">
        <span class="sr-only">toggle menu</span>
        <span class="icon-phone"></span>
      </button>

        <?php
        if (has_nav_menu('left_navigation')) :
          wp_nav_menu(['theme_location' => 'left_navigation', 'items_wrap' => '<ul id="toggle-%1$s">%3$s</ul>' ]);
        endif;
        ?>  


      <a class="brand" data-scroll href="#het-koor">      
          <?php get_template_part('templates/logo'); ?>
      </a>

        <?php
        if (has_nav_menu('right_navigation')) :
          wp_nav_menu(['theme_location' => 'right_navigation','items_wrap' => '<ul id="toggle-%1$s">%3$s</ul>' ]);
        endif;
        ?>    

    </nav>
  </div>
</header>
