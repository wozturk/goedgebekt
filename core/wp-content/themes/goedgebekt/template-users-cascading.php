<?php
/**
 * Template Name: Koorleden Pagina Alternatief
 */
?>


<article class="flex-box row" id="content-<?php echo $post->post_name;?>">
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'users-cascading'); ?>
</article>

