<?php

$args = array( 
  'post_type' => 'page', 
  'orderby' => array(
  'menu_order' => 'ASC'),
  'post__not_in'=> array(7, 17)
  );
$page_query = new WP_Query( $args ); ?>

<div class="sections">

  <?php if ( $page_query-> have_posts() ) : while ($page_query-> have_posts() ) : $page_query-> the_post(); 
    $template_file = get_post_meta( get_the_ID(), '_wp_page_template', TRUE ); 
    ?>

    <div id="<?php echo $post->post_name;?>" class="section section--<?php echo $post->post_name;?>" data-anchor="<?php echo $post->post_name;?>">
    <?php 
      if ($template_file=='default') { 
          include 'page.php'; 
      } else {
        require $template_file;
      }
    ?>
     <?php if (get_field('toon_quotes')) :?>
      <div class="quotes">
        <?php get_template_part('templates/quotes'); ?>  
      </div>
    <?php else: ?>
      <div class="spacer"></div>
    <?php endif; ?>
    <?php 
      $background = new HarperJones\Wordpress\Media\FeaturedImage($post->ID, 'full');
      // $backgroundP = new HarperJones\Wordpress\Media\Attachment(187, 'full');


      // $background->displayPicture(['bg-img'],'Goed Gebekt','full');

    ?>
    <picture>
      <source srcset="http://goedgebekt.wietekeozturk.com/core/wp-content/uploads/2016/12/gg_portrait.jpg" type="image/jpeg" media="(orientation:portrait)">
       <?php // $background->displaySrc('(orientation:portrait)'); ?> 
      <?php echo $background->displayImg('full', ['bg-img'], true); ?>
    </picture>
    </div>
  <?php endwhile;       
  endif; 
  wp_reset_postdata(); ?>

</div/>
  


      


 