<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>  data-anm=".anm">
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('<strong>Seriously?</strong>Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');?>
      <div data-loaded>
        <div data-loading>
          <?php get_template_part('templates/notes'); ?>
        </div>
      </div>
      <?php get_template_part('templates/header');
      get_template_part('front-page');
      get_template_part('templates/footer');
      do_action('get_footer');
      wp_footer();
    ?>
  </body>
</html>
