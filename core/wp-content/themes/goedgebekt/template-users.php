<?php
/**
 * Template Name: Koorleden Pagina
 */
?>


<article class="flex-box row" id="content-<?php echo $post->post_name;?>">
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'users'); ?>
</article>
  <?php get_template_part('templates/content', 'addons'); ?>