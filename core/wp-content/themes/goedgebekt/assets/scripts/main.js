
/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {


  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
          $(window).load(function() { 
            $('[data-loading]').fadeOut(200); 
            $('[data-loaded]').delay(150).fadeOut(500);
            $(document.body).delay(150).css({'overflow':'visible'});
          });

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // Javascript to be fired on the home page
        
        $(document.body).on('click', '[data-scroll]', function(e) {
          var href = $.attr(this, 'href');
          e.preventDefault();
          $('html, body').animate({
              scrollTop: $(href).offset().top - 100
          }, 500, function () {
            window.location.hash = href;
          });
        });

        
        $("[data-carousel]").slick({
          autoplay: true,
          autoplaySpeed: 5000,
          slidesToShow: 1,
          arrows: false,
          draggable: false,
          pauseOnFocus: false,
          pauseOnHover: false
        });

       // shuffle function for repertoire
        var Shuffle = window.shuffle;
        var element = $('[data-shuffle]');
        
        var shuffle = new Shuffle(element, {
          itemSelector: '[data-shuffle] li',
          sizer: '.list__item' 
        });

        //extend methods
        $.fn.extend({
            fixImage: function() {
              $(this).find('.bg-img').css('position', 'fixed');
            },
            unFixImage: function() {
              $(this).find('.bg-img').css('position', 'absolute');
            },
            animateCss: function (animationName) {
                var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                this.addClass('animated ' + animationName).one(animationEnd, function() {
                    $(this).removeClass('animated ' + animationName).css('opacity', '1');
                });
            },
            removeCss: function(animationName) {
                this.removeClass('animated ' + animationName ).css('opacity', '0');

            },
            doAnimation: function() {
              this.find('[data-animate]').each(function() {
                animateFX = $(this).data('animate');
                $(this).animateCss(animateFX);
              });
            },
            resetAnimation: function() {
              this.find('[data-animate]').each(function() {
                animateFX = $(this).data('animate');
                $(this).removeCss(animateFX);
              });
            }
        });

        //header moves to top
        $('[data-anchor="het-koor"]').waypoint({
          handler: function(direction) {
            if (direction === 'up') {
              $('.header').addClass('header--floating');
              window.location.hash = '';
              $('.js-notes').css('display', 'block').children().addClass('animated');
            } else {
              $('.header').removeClass('header--floating');
              $('.js-notes').css('display', 'none').children().removeClass('animated');
            }
          },
          offset: '60%',
        });

        function removeFilter() 
        {
          $('[data-group]').removeClass('active').parent().removeClass('filter-is-on');
          shuffle.filter();
        }

        $('[data-anchor]').waypoint({
          handler: function(direction) {
            if(direction === 'down') {
              $(this.element).fixImage();
            } else {
              $(this.element).unFixImage();
            }
          },
          offset: 0,
        });


        // add animation to section on scroll into view
        $('[data-anchor]').waypoint({
          handler: function(direction) {
            if (direction === 'down') {
              $(this.element).doAnimation();  
            } else {
              $(this.element).resetAnimation();
            }
          },
          offset: '60%',
        });

        $(document).on('click','[data-group]', function(e) {
          var selectedGroup = $(this).data('group'),
              active        = $(this).hasClass('active');
            
          $('[data-group]').removeClass('active');

          if ( active ) {
            shuffle.filter();
            $(this).parent().removeClass('filter-is-on');
            // $(this).removeClass('active');
          } else {
            shuffle.filter(selectedGroup);
            $(this).parent().addClass('filter-is-on');
            $(this).addClass('active');
          }
          e.preventDefault();
        });
            
        // strip contactform 7 of spans, add some fx to input fields when value is entered
        var $inputs = $('.wpcf7 input'),
            $textareas = $('.wpcf7 textarea'),
            $list = $inputs.add($textareas);
        
        $list.unwrap(); 



        $list.each(function() {
          $(this).append('<span class="wpcf7-form-control-wrap' + $(this).attr('name') + '></span>');
          $(this).blur(function() {
            if ($(this).val()) {
              $(this).addClass('ready');
            }
          });
        });


        //add data-animate to wordpress generated anchors
        $('.tag-cloud a').each(function(e) {
          $(this).attr('data-animate', 'bounceIn');
        });


        //toggle for show-hiding stuff
        
        function toggleFocus($focus, target) {
          var focusGroup   = $focus.data('focus'),
          $focusParent = $focus.closest('[data-focus-parent]'),
          $siblings   = $focusParent.find('[data-focus=' + focusGroup + ']').add($('[id^="toggle-"]')),
          $list       = $('[data-target=' + target + '], #' + target).add($focus).add($focusParent),
          className   = 'focus';
          
          if ($focus.hasClass(className)) {
            $siblings.removeClass(className);
            $list.removeClass(className);
          } else {
             $siblings.removeClass(className);
            $list.addClass(className);
          }
        }

        $(document.body).on('click', '[data-focus]' , function(e) {
          var $focus = $(this),
              target = $focus.data('target');
          toggleFocus($focus, target);
          e.preventDefault();
        });

        $(document.body).on('click', '[id^="toggle-"] [data-target]', function(e) {
          $(this).parents('[data-focus-parent]').childeren('.focus').removeClass('focus');
          e.preventDefault();
        });

        //animate notes on mousemove
        anm.off();
        function moveNotes() {
          $(document.body).on('mousemove mouseleave', '[data-anm-on]', function(e) {
            if (e.clientX <= window.innerWidth/2 && e.type === 'mousemove') {
              anm.on();
            } else {
              $('.anm').css('transform', 'translate3d(0,0,0)');
              anm.off();
            }
          });       
        }
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
