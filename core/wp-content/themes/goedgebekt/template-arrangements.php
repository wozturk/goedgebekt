<?php
/**
 * Template Name: Repertoire Pagina
 */
?>


<div class="container-fluid">
  <article class="flex-box" id="content-<?php echo $post->post_name;?>">
    <?php get_template_part('templates/page', 'header'); ?>
    <?php get_template_part('templates/content', 'arrangements'); ?>
  </article>
</div>
