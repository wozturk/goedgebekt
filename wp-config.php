<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

require_once( __DIR__ . '/vendor/autoload.php');
/**
 * Expose global env() function from oscarotero/env
 */
Env::init();



$dotenv = new Dotenv\Dotenv(__DIR__);

if (file_exists(__DIR__ . '/.env')) {
    $dotenv->load();
}

define('WP_HOME', env('WP_HOME'));

define('WP_SITEURL', env('WP_SITEURL'));


// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', env('DB_NAME'));

/** MySQL database username */
define('DB_USER', env('DB_USER'));

/** MySQL database password */
define('DB_PASSWORD', env('DB_PASSWORD'));

/** MySQL hostname */
define('DB_HOST', env('DB_HOST'));

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8nM0+|V`W]DX*k=D8N&b+W-jZF1<nwLNFS&n>LFe4p?-.[6;/k7VR5E@??([K0,d');
define('SECURE_AUTH_KEY',  'L~=57yvAcYiEl6b+zeY!u=]??P=-Yy`1y~N~jnbt~:+Y z,!cl<ebZ6fIHZBFlp/');
define('LOGGED_IN_KEY',    '(BBP3oQ||6b~pI8&.Eyf7._9dIB2T:-z($A)MlFtuW5:s{1zL+},|%J,HZT41&+h');
define('NONCE_KEY',        'KQx!qi*#Mb /h0.wZ0XHedA$%K$IDV(x;.5#c*!5S%EONzL31-im=KFRPSNC-zK ');
define('AUTH_SALT',        'xp0GABGh[=3&gd_MGvtse?]4P+UI%]eK(bM,b3O]eaV$a<*cu]:b1rYPo~`Mo9^/');
define('SECURE_AUTH_SALT', 'btjaf?GIdp+S=g_!mlmH9hkeYW`UP3<ybph1mXYsTn9^p0a4g[0rb_s&rG |@J+*');
define('LOGGED_IN_SALT',   '(h/CG~(Kv5UL(&`=Mlc2GgH6%hDb>i!(KB_OFJTxB;7g[[+}:jc[&#QbGJb?L7rh');
define('NONCE_SALT',       'JB_Mu;-;y/<lRll/&<f;^P`_F4Jq1]xAS3j<tF&*t;8(SW3?pLHDF4Ihvr/l4eN*');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
